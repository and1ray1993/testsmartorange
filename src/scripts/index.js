$(document).ready(() => {

  const sliderItem = $('.js-slider');
  const sliderNavItem = $('.js-nav-slider');

  sliderItem.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.js-nav-slider'
  });

  sliderNavItem.slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.js-slider',
    focusOnSelect: true,
    arrows: true,
    prevArrow: '.js-arrow-prev',
    nextArrow: '.js-arrow-next',
    variableWidth: true
  });

  sliderItem.on('afterChange', function checkCurrent() {
    const $slides = $('.js-item');
    const $navSlides = $('.js-nav-item');

    $slides.each((i, item) => {
      if ($(item).hasClass('slick-current')) {

        const index = $(item).children().attr('data-index');

        $navSlides.each((i, item) => {
          $(item).removeClass('slick-current');
          if (index === $(item).children().attr('data-index')) {
            $(item).addClass('slick-current');
          }
        });

      }
    });
  });

  $('.js-zoom').zoom({
    magnify: 2
  });

  function popup(classPopup) {
    const $popup = $(classPopup);
    const $closeBtnPopup = $('.js-close-popup');
    $popup.addClass('popup__active');
    $closeBtnPopup.click(() => $popup.removeClass('popup__active'));
  }

  $('.js-buy').click(() => {
    $('.js-buy')
      .css({'background':'green'})
      .text('В корзине')
      .attr('disabled', 'true');

    popup('.js-popup');
  });
});